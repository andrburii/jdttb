<?php

// =====================================================================================
//                                   Clases
// =====================================================================================

class DataBase
{
  protected $db;
  public $ArrayType = array(
   "DVD" => DVD::class,
   "Book" => Book::class,
   "Furniture" => Furniture::class
   );

  public function __construct() {
      if (!$this->db) $this->db = new mysqli('localhost','id18423229_andrburii','Lj{g_7(T5SDxB#@','id18423229_jdttb');
      if (!$this->db) {
         die('Connection Error: ' . mysql_error());
         }
  }

  public function Run_query($query){
    if($query) {
       $result = $this->db->query($query);
       return $result;
   } else {$result = "";}
  }

 function __destruct() {
        if ($this->db) $this->db->close();
    }
}


abstract class Product
{
  private $NPP;
  private $SKU;
  private $Name;
  private $Price;
  private $ProductType;

  public function SetData($array)
  {
    if (isset($array['NPP'])) $this->NPP = $array['NPP'];
    $this->SKU = $array['SKU'];
    $this->Name = $array['Name'];
    $this->Price = $array['Price'];
    $this->ProductType = $array['ProductType'];

  }

  public function GetData()
  {
    return get_object_vars($this);
   }

  public function ProductInfo($num)
  {
    return ' <div class="product">
                    <input type="checkbox" class="delete-checkbox" name="Delete'.$num.'" value='.$this->NPP.'>
                    <p class="sku" id="SKU">'.$this->SKU.'</p>
                    <p class="name" id="Name">'.$this->Name.'</p>
                    <p class="price" id="Price">'.number_format($this->Price, 2).' $</p>
                    <p class="attribute" id="Attribute">'.$this->GetAttribute().'</p>
            </div>';
  }

  abstract protected function GetAttribute();
}

class DVD extends Product
{
    private $Size;

    public function SetData($array)
    {
      parent:: SetData($array);
      $this->Size = $array['Size'];
    }
    
    public function GetData()
      {
          $ArParent = parent::GetData();
          $ArChild = get_object_vars($this);
          
        return array_merge($ArParent,$ArChild);
      }   

    public function GetAttribute()
        {
          return  'Size: '.$this->Size.' MB';
        }

}

class Book extends Product
{
    private $Weight;

    public function GetAttribute()
        {
          return  'Weight: '.$this->Weight.' KG';
        }

    public function SetData($array)
        {
          parent:: SetData($array);
          $this->Weight = $array['Weight'];
        }
   
    public function GetData()
      {
          $ArParent = parent::GetData();
          $ArChild = get_object_vars($this);
          
        return array_merge($ArParent,$ArChild);
      }     

}

class Furniture extends Product
{
    private $Height;
    private $Width;
    private $Length;

    public function GetAttribute()
        {
          return  'Dimension: '.$this->Height.'x'.$this->Width.'x'.$this->Length.'CM';
        }

    public function SetData($array)
        {
          parent:: SetData($array);
          $this->Height = $array['Height'];
          $this->Width = $array['Width'];
          $this->Length = $array['Length'];
        }
        
    public function GetData()
      {
          $ArParent = parent::GetData();
          $ArChild = get_object_vars($this);
          
        return array_merge($ArParent,$ArChild);
      }  

}
// =====================================================================================

class ViewList extends DataBase
{
  private $ProdType;

  public function ViewData(){
    $data = $this->Run_query('SELECT * FROM Product');

    $showpub = '<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <link rel="preconnect" href="https://fonts.googleapis.com">
                    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
                    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
                    <link rel="stylesheet" href="style.css">
                    <title>JDTTB</title>
                </head>
                <body>
                <div class="body">
                        <div class="main">
                        <form name="ListProduct" action="" method="POST">
                        <input type="text" name="Delete" hidden>
                            <div class="nav">
                               <div class="title"><br><span class="title-text">Product List</span>
                                   <span class="buttons">
                                        <button type="submit" formaction="/add-product.html">ADD</button>
                                        <button type="submit" id="delete-product-btn" >MASS DELETE</button>
            
                                </div>
                            </div>
                            <div class="product-list">';
    $num = 0;

    while($row = $data->fetch_assoc()) {
      $this->ProdType = new $this->ArrayType[$row['ProductType']];
      $this->ProdType->SetData($row);
      $showpub .= $this->ProdType-> ProductInfo($num);
      $num++;
    };

    $showpub .='</div>
                </form>
                <div class="footer">Scandiweb Test Assignment</div>
            </div>
        </div>
        </body>
        </html>';
    return $showpub;
  }
}

// =====================================================================================

class State extends DataBase
{
  function __construct() {
     parent::__construct();
   }

   private $ProdType;

   private function AddProduct(){

     $vars_class = $this->ProdType->GetData();
     $querry = 'INSERT INTO Product (';
     $qvalues = ') VALUES (';

     foreach($vars_class as $key => $value)
        {
            if ($key != 'NPP'){
              $querry .= $key.',';
              $qvalues .= '"'.$value.'",';
            }
        }
        $querry = substr($querry, 0, -1);
     $querry .= substr($qvalues, 0, -1).')';
     $this->Run_query($querry);
   }

   private function RemoveProduct($data){
     foreach($data as $entry){
         $values = $entry;
         if ($values){
         $querry = 'DELETE FROM Product WHERE NPP='.$values;
           $this->Run_query($querry);
         }
     }
   }

  public function def($array){
    $Kil = count($array);
    if ($Kil > 0){
        if (isset($array["Delete"])){
            $this->RemoveProduct($array);
        } else
        if (isset($array["Add"])){
          $this->ProdType = new $this->ArrayType[$array['ProductType']];
          $this->ProdType->SetData($array);
          $this->AddProduct();
        }
    }

  }
}

// =====================================================================================
//                                Program
// =====================================================================================
$ST = new State;
$ST->def($_POST);
$ListProduct  = new ViewList;
$show = $ListProduct->ViewData();
echo $show;

?>
