window.onload=function(){
    typeSwitcher();
productType.addEventListener('change',typeSwitcher );
function typeSwitcher(){
    //console.log(productType.value);
    var elem = document.getElementById('length');
    switch (productType.value){
        case 'DVD':
            DVD.setAttribute('style', 'display:block');
            Book.setAttribute('style', 'display:none');
            Furniture.setAttribute('style', 'display:none');
            size.setAttribute('required', true);
            height.removeAttribute('required');
            width.removeAttribute('required');
            elem.removeAttribute('required');
            weight.removeAttribute('required');
            break;
        case 'Book':
            DVD.setAttribute('style', 'display:none');
            Book.setAttribute('style', 'display:block');
            Furniture.setAttribute('style', 'display:none');
            size.removeAttribute('required');
            height.removeAttribute('required');
            width.removeAttribute('required');
            elem.removeAttribute('required');
            weight.setAttribute('required', true);
            break;
        case 'Furniture':
            DVD.setAttribute('style', 'display:none');
            Book.setAttribute('style', 'display:none');
            Furniture.setAttribute('style', 'display:block');
            size.removeAttribute('required');
            height.setAttribute('required', true);
            width.setAttribute('required', true);
            elem.setAttribute('required', true);
            weight.removeAttribute('required');
            break;

    }   
}
};
